kwitty is a local, simple and tab based twitter web application for chrome.

Features:
*Login with OAuth or API proxy (GAE).
*Official retweet or non-official RT.
*Click to show one level reply inline.
*More info (tweet freqency, following you or not) about other users.
*Expand shortened URLs and show rich content (photo, video etc.) inline.
*Compact view when open as window. 
*24 kinds of timeline themes.
*Background customization.
*Export/Backup anyone's timeline (max 3200 tweets limited by Twitter) to html. Then you can save it as (all content) a local file.

Tips:
*Commands in tweet box: 
    direct message: d user content
    find user: f username
    search tweets: s keyword
*Click the black top bar to go to top.
*The URL link will be detected automatically and be shortened to "to.co".
*App mode: select "open as window" and create a shortcut.

If you have problems, please feel free to get in touch with me (@killwing) or go to "Support" on the right side.
